#include <iostream>

using namespace std;

//A11.2021.13887 - Timothy Mulya Cahyana

int main () 
{
    int n;
    
    cout << "Masukan angka: "; cin >> n;
    //Note: 0, 1 bukanlah bilangan prima

    for (int i = 2; i < n; i++) 
    {
        bool prime = true; 
        for (int m = 2; m*m <= i; m++)  
        {
           if (i % m == 0) 
            {
                prime = false; 
                break;   
            }
        }   
        if(prime) cout << i << ", "; 
    }

    bool prime2 = true; 
    if (n == 0 || n == 1){
        prime2 = false;
    }
    else {
        for (int i = 2; i <= n / 2; i++) { 
            if (n % i == 0) {
                prime2 = false;
                break;
            }

        }
    }

    if(prime2) {
        cout << "Angka yang anda masukan tadi, " << n <<  " adalah bilangan prima\n\n";
    }
    else{
        cout << "AngkaYang yang anda masukan tadi, " << n << " bukanlah bilangan prima\n\n";
    }

    return 0;
}