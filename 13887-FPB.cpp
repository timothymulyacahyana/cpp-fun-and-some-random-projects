#include <iostream>

using namespace std;

//A11.2021.13887 - Timothy Mulya Cahyana

int main() {
  int nwan, ntwo, fpb, swan; 

  cout << "Masukan 2 angka: "; cin >> nwan;
  cout << "Dan: "; cin >> ntwo;

  cout << "\n" << "Faktor dari " << nwan << " = "; 
  
  //kode ini sama dasarnya dengan kode kode dibawah, alasan tidak disatukan: hasil dari faktornya itu berderet, harus dilooping

  for (int i = 2; i <= nwan; i++){
      if (nwan % i == 0){
          cout << i << ", ";
      }
    }

  cout << "\n" << "Faktor dari " << ntwo << " = "; 
  for (int i = 2; i <= ntwo; i++){
      if (ntwo % i == 0){
          cout << i << ", ";
      }
    }

  for (int i = 1; i <= nwan && i <= ntwo; i++) { //Code dasar bisa dipisah untuk mencari faktor seperti kode diatas sebelum code ini
    if (nwan % i == 0 && ntwo % i ==0) {
      fpb = i;
    }
  }

  cout << "\n" << "Maka FPB dari " << nwan << " dan " << ntwo << " adalah " << fpb;
  
  return 0;
}