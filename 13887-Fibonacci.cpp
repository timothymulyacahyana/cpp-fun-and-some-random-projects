#include <iostream>

using namespace std;

//A11.2021.13887 - Timothy Mulya Cahyana

int main(){
    int n, fn = 0, sn = 1, res;
    
    cout << "Masukan jumlah angka fibonacci yang anda ingin lihat: "; cin >> n;
    cout << "\n" << "Ini angkanya: "; 

    for(int i = 0; i <= n; i++){

        cout << fn << ", "; //cout "0" lalu dilanjukan dengan hasil loop setelahnya

        res = fn + sn; // memindahkan Hasil
        fn = sn; // 0 menjadi 1, 1, 2, 3, dst
        sn = res; // 1 menjadi 0 + 1, 1 + 1, 1 + 2, 2 + 3, dst
    }
    return 0;
}