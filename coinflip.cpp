#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std;

int main()
{
	srand(time(NULL));
	int coin = rand() % 2;

	if (coin == 0) {
		cout << "Head\n";
	}
	else {
		cout << "Tail\n";
	}
	return 0;
}