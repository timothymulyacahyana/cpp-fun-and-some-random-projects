#include <iostream>  
#include <cmath>

using namespace std; 

//NOTE! THIS CAN ONLY CONVERT UP TO 1023 

long long convert(int); // this is the identifier of convert

int main()
{
	int n, binary; //Note: int can only hold whole numbers, cant hold fractions or decimals
	cout << "Enter your decimals: "; 
	cin >> n; //Input the decimal numbers
	binary = convert(n); //value of binary
	cout << " " << n << " Has been converted to: " << binary << "\n";
	return 0;
}

long long convert(int n) {
	long long binary = 0; //the bin initial value is 0
	int rem, i = 1;

	while (n != 0) // While will loop the equations until the result of n is 0
	{
		rem = n % 2; // Rem-ainder of n is( its either 0 or 1 just like the binary) and the result will be calculated to (binary += rem * i)
		n /= 2; // Value of n will be looped back for the next equation to search the value of rem
		binary += rem * i; //The value of the binary is a summation of the remainder of the previous loop - ```binary = rem+1``` * i( the value of i is updated after previous loop)
		i *= 10; // Multiplies the value of i to be use for the next loop ```1*10=i
	}
	return binary; //This will return the value of convert 
}